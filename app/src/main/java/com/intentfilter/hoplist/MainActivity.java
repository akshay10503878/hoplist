package com.intentfilter.hoplist;

import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class MainActivity extends AppCompatActivity {
    private ListView mListView;
    private String targetServer = "www.google.com";
    private static final String baseDataDir = "/data/data/com.intentfilter.hoplist/";

    private Pattern VALID_IPV4_PATTERN = null;
    private Pattern VALID_IPV6_PATTERN = null;
    private final String ipv4Pattern = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
    private final String ipv6Pattern = "([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}";
    private List<String> listItems;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override

    public void onCreate(Bundle savedInstanceState) {

        listItems=new ArrayList<String>();
        //super.onCreate(savedInstanceState, persistentState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = (ListView) findViewById(R.id.Hops_list_view);

        try {
            InputStream tris = getAssets().open("traceroute");
            byte[] trbuffer = new byte[tris.available()];
            tris.read(trbuffer);
            tris.close();
            FileOutputStream trfos = new FileOutputStream(
                    baseDataDir + "traceroute");
            trfos.write(trbuffer);
            trfos.close();

            File trFile = new File(baseDataDir + "traceroute");
            if (trFile.exists()) {
                trFile.setExecutable(true);
            }

        } catch (Exception e) {

        }

        try {
            VALID_IPV4_PATTERN = Pattern.compile(ipv4Pattern, Pattern.CASE_INSENSITIVE);
            VALID_IPV6_PATTERN = Pattern.compile(ipv6Pattern, Pattern.CASE_INSENSITIVE);
        } catch (PatternSyntaxException e) {

        }

// 1
        Thread tracerouteThread = new Thread(new Runnable() {
            @Override
            public void run() {
                runTraceCommand();
            }
        });
        tracerouteThread.start();

        /*
        final ArrayList<Recipe> recipeList = Recipe.getRecipesFromFile("recipes.json", this);
// 2
        String[] listItems = new String[recipeList.size()];
// 3
        for(int i = 0; i < recipeList.size(); i++){
            Recipe recipe = recipeList.get(i);
            listItems[i] = recipe.title;
        }
// 4
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems);
        mListView.setAdapter(adapter);

*/

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void runTraceCommand() {
        InputStream tracerouteIS = null;
        // 1. Create an InetAddress object using getByName passing the "targetServer" command parameter.
        InetAddress targetInetAddress = null;
        try {
            targetInetAddress = InetAddress.getByName(targetServer);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        // 2. Run the traceroute command providing the IP address we get from above.
        Process execProc = null;
        BufferedReader trReader = null;
        try {
            String[] tracerouteCommand = {"/data/data/com.intentfilter.hoplist/traceroute", targetInetAddress.getHostAddress(), "-n", "-m", "30", "-q", "3", "-p", "33434"};
            ProcessBuilder builder = new ProcessBuilder(tracerouteCommand);
            execProc = builder.redirectErrorStream(true).start();
            Thread.sleep(1000, 0);

            // 3. Parse the output of the command.
            tracerouteIS = execProc.getInputStream();

            if (tracerouteIS.available() > 0) {
                trReader = new BufferedReader(new InputStreamReader(tracerouteIS));

                try {
                    String outputString = null;
                    while ((outputString = trReader.readLine()) != null) {
                        if (outputString.isEmpty()) {
                            continue;
                        }

                        if (outputString.startsWith("traceroute")) {
                            continue;
                        }

                        outputString = outputString.replaceAll("ms", "");
                        outputString = outputString.replaceAll("  ", " ");
                        outputString = outputString.replaceAll("  ", " ");
                        String[] tempOutputStrings = outputString.trim().split(" ");

                        String[] outputStrings = null;
                        List<String> outputStringsArray = new ArrayList<String>();

                        if (tempOutputStrings.length > 4) {
                            outputStringsArray.add(tempOutputStrings[0]);
                            outputStringsArray.add(tempOutputStrings[1]);
                            for (int i = 2; i < tempOutputStrings.length; i++) {
                                //if(!(tempOutputStrings[i].split(".").length > 1)) {
                                if (!isIpAddress(tempOutputStrings[i].trim())) {
                                    outputStringsArray.add(tempOutputStrings[i]);
                                }
                            }
                            outputStrings = outputStringsArray.toArray(new String[0]);
                        } else {
                            outputStrings = tempOutputStrings;
                        }

                        System.out.println(outputString);

                        //    Log.d("TracerouteCommand", "###" + outputString);

                        // 4. Populate the db with the parsed values.
                        int timeToLive = Integer.parseInt(outputStrings[0].trim());
                        String nodeIP = "";
                        String rtt1 = "";
                        String rtt2 = "";
                        String rtt3 = "";
                        if (outputStrings.length < 5) {
                            nodeIP = "";
                            rtt1 = "*";
                            rtt2 = "*";
                            rtt3 = "*";
                        } else {
                            nodeIP = outputStrings[1];
                            rtt1 = outputStrings[2];
                            rtt2 = outputStrings[3];
                            rtt3 = outputStrings[4];
                        }
                        listItems.add(nodeIP);
                        Log.d("hop=>", nodeIP);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            execProc.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (trReader != null)
                try {
                    trReader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            if (tracerouteIS != null)
                try {
                    tracerouteIS.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String[] Items = new String[listItems.size()];
                    Items=listItems.toArray(Items);
// 3
                    //stuff that updates ui
                    ArrayAdapter adapter = new ArrayAdapter<String>(MainActivity.this,R.layout.activity_listview,Items);

                    // ArrayAdapter adapter = new ArrayAdapter(this,Items);
                    mListView.setAdapter(adapter);

                }
            });

        }
    }

    /**
     * Determine if the given string is a valid IPv4 or IPv6 address.  This method
     * uses pattern matching to see if the given string could be a valid IP address.
     *
     * @param ipAddress A string that is to be examined to verify whether or not
     *                  it could be a valid IP address.
     * @return <code>true</code> if the string is a value that is a valid IP address,
     * <code>false</code> otherwise.
     */
    public boolean isIpAddress(String ipAddress) {

        Matcher m1 = VALID_IPV4_PATTERN.matcher(ipAddress);
        if (m1.matches()) {
            return true;
        }
        Matcher m2 = VALID_IPV6_PATTERN.matcher(ipAddress);
        return m2.matches();
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
